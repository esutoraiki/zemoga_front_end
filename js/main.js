/*global document, window, console, $c */
/*jslint maxlen: 100 plusplus: true nomen: true */

/**
* namespace
*/
var centroNS = (function () {
    "use strict";

    var last_class;

    function close_submenu(event) {
        $c.Eve.pd(event);
        $c.Eve.sp(event);

        this.className = last_class.replace(" show", "");
    }

    function open_submenu(event) {
        var nodo = this.lastElementChild;

        last_class = this.lastElementChild.className;
        last_class = last_class.replace(" show", "");
        nodo.className = last_class + " show";

        $c.$("." + last_class)
            .click(close_submenu)
            .to("mouseleave", close_submenu);

        $c.Eve.pd(event);
    }

    return {
        submenu: function () {
            $c.$(".open_submenu")
                .click(open_submenu)
                .to("mouseover", open_submenu);

        }
    };
}());

$c.load(function () {
    "use strict";
    centroNS.submenu();
    var slider_1 = new Slider();
});
