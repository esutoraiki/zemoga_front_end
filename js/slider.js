/*global document, window, console, $c */
/*jslint maxlen: 100 plusplus: true nomen: true */

/* Nota: Si se mantiene moviendo el mouse sobre el slider este 
 * se mantendra detenido hasta que pare de morvese
 * reiniciando el conteo de la función auto */


//construct
function Slider(attr_e) {
    "use strict";

    var attr_e = attr_e || {},
        attr = {};

    $c.extend({
        id         : attr_e.id || "slider",
        pointer    : 1,
        num        : $c.$(".slider_item").N.length,
        class_item : attr_e.class_item || "slider_item",
        time       : attr_e.time || 5000,
        set_time   : null,
        startx     : null,
        dist       : null
    }, this);

    this.init();
}

//prototype
Slider.prototype = {
    init: function () {
        "use strict";

        this.slider_tool(); //configuration nex and prev buttom
        this.mousemove();   //Lock slider by moving the mouse
        this.touch();       //Events touch
        this.auto();        //automatic movement of the slider
    },

    stop_auto: function () {
        window.clearTimeout(this.set_time);
    },

    change_active: function (direction_e) {
        var direction = direction_e || false;

        this.stop_auto();
        this.change_pointer(direction);
        for (i = 1; i <= this.num; i++) {
            $c.$("#slider_" + i).N.className = this.class_item;
        }

        $c.$("#slider_" + this.pointer).N.className = this.class_item + " active";

        this.auto();
    },

    slider_tool: function () {
        var i;

        $c.$(".next").click(function (){
            this.change_active();
        }.bind(this));

        $c.$(".prev").click(function (){
            this.change_active(true);
        }.bind(this));
    },

    change_pointer: function (direction_e) {
        var direction = direction_e || false;

        if (direction) {
            this.pointer--;

            if (this.pointer <= 0) {
                this.pointer = this.num;
            }
        } else {
            this.pointer++;

            if (this.pointer > this.num) {
                this.pointer = 1;
            }
        }
    },

    auto: function () {
        //No use window.setinterval por eficiencia
        this.set_time = window.setTimeout(function () {
            this.change_active();
        }.bind(this), this.time);
    },

    mousemove: function () {
        $c.$("#slider").to("mousemove", function() {
            this.stop_auto();
            this.auto()
        }.bind(this));
    },

    touch: function () {
        var container = $c.$("#container_slider").N;

        container.addEventListener("touchstart", function (e) {
            var touchobj = e.changedTouches[0];
            this.startx = parseInt(touchobj.clientX);
        }.bind(this));

        container.addEventListener("touchmove", function (e) {
            var touchobj = e.changedTouches[0];
            this.dist = parseInt(touchobj.clientX) - this.startx;
        }.bind(this));

        container.addEventListener("touchend", function (e) {
            if (this.dist > 50) {
                this.change_active();
            } else if (this.dist < -50) {
                this.change_active(true);
            }
        }.bind(this));
    }
}
