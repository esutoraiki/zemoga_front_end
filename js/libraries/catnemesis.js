/* *********************************
* 
* catnemesis.js
* Funciones varias para trabajar con JavaScript
* 
* Autor         : John Sánchez Alvarez
* Creación      : 06/08/2013
* Actualización : 18/03/2015
* 
* Scope  : window
* Objeto : $c
* 
* Atributos:
* N: Almacenar el último objeto encontrado con el método $
* H: Array con historial de objetos encontrados con el método $
* activo_H: Permite activar (true) o desactivar (false) a H
*  
* Metodos: 
* - limpiar(): Inicializa los atributos de $c
* - extend(atributos, objeto): Extiende un objeto
* - $(selector, atributos): selector
* - text(contenido): Realiza innerText a un nodo
* - html(contenido): Realiza innerHTML a un nodo
* - mostrar_ocultar(atributos): Mostrar u ocultar
* - nn(valor, atributos): Dispara la funcion según la comparacion
* - keycode(event): Obtener keycode
* - keylock(event, key_code) : Bloquea un conjunto de teclas
* - Eve: [object] Disparador de eventos
* 
* Alias:
* toggle, load, keydown, click, to, remove_event, resize
*********************************** */

/*global document, window, console */
/*jslint maxlen: 100, plusplus: true, unparam: true */

(function () {
    "use strict";

    var lastTime = 0,
        vendors = ["ms", "moz", "webkit", "o"],
        x;

    for (x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + "RequestAnimationFrame"];
        window.cancelAnimationFrame  = window[vendors[x] + "CancelAnimationFrame"]
                                   || window[vendors[x] + "CancelRequestAnimationFrame"];
    }

    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function (callback, element) {
            var currTime = new Date().getTime(),
                timeToCall = Math.max(0, 16 - (currTime - lastTime)),
                id = window.setTimeout(function () {
                    callback(currTime + timeToCall);
                }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }

    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function (id) {
            window.clearTimeout(id);
        };
    }
}());

//OBJETO $c
var $c = (function () {
    "use strict";

    function eliminar_string(selector, elimina) {
        return selector.split(elimina).join("");
    }

    return {
        N: null, //Atributo para almacenamiento de objeto selecionado actual
        H: [], //Mantiene un historial de objetos seleccionados
        activo_H: false, //Activa/desactiva historial

        limpiar: function () {
            this.N = null;
            this.H = [];
            this.activo_H = false;
        },

        /**
         * Extendiendo Objetos
         * @param   {Object} a_e Atributos y métodos que se desean agregar al objeto
         * @param   {Object} o_e Objeto a extender
         * @returns {Object} Objeto extendido
         */
        extend: function (a_e, o_e) {
            var a = a_e || {},
                o = o_e || this,
                x;
            for (x in a) {
                if (a.hasOwnProperty(x)) {
                    o[x] = a[x];
                }
            }
            return o;
        },

        /**
        * Selector. Scope: $c.$();
        * @param   {String} selector Ejemplo: #id
        * @param   {Object} nodo_e   Nodo selecionado, por defecto document
        * @returns {Object} Concatenador de metodos
        */
        $: function (selector, nodo_e) {
            var nodo = nodo_e || document,
                salida = null,
                tipo;

            //Almacenar objeto o nodo anterior seleccionado
            if (this.N !== null && this.activo_H) {
                this.H.push(this.N);
            }

            //Determina el tipo de selector
            tipo = selector.charAt(0);

            switch (tipo) {
            case "#":
                selector = eliminar_string(selector, tipo);
                salida = nodo.getElementById(selector);
                break;
            case "%":
                selector = eliminar_string(selector, tipo);
                salida = nodo.getElementsByName(selector);
                break;
            case ".":
                selector = eliminar_string(selector, tipo);
                salida = nodo.getElementsByClassName(selector);
                break;
            default:
                salida = nodo.getElementsByTagName(selector);
                break;
            }

            this.N = salida; //Almacenando el ultimo nodo
            return this;
        },

        /**
         * Realiza innerText a un nodo
         * @param   {String} contenido_e Texto del nodo
         * @returns {Object} Concatenador de métodos
         */
        text: function (contenido_e, concatenado_e) {
            var contenido   = contenido_e || null,
                concatenado = concatenado_e || false;

            if (this.N !== null && contenido !== null) {
                if (this.N.innerText) {
                    this.N.innerText = concatenado
                        ? this.N.innerText + contenido : contenido;
                } else {
                    this.N.textContent = concatenado
                        ? this.N.textContent + contenido : contenido;
                }
            } else if (this.N !== null && contenido === null) {
                if (this.N.innerText) {
                    return this.N.innerText;
                }
                return this.N.textContent;
            }
            return this;
        },

        /**
         * Realiza innerHTML a un nodo
         * @param   {String}  contenido_e   HTML del nodo
         * @param   {Boolean} concatenado_e Concatenado de contenido
         * @returns {Object}  Concatenador de métodos
         */
        html: function (contenido_e, concatenado_e) {
            var contenido   = contenido_e || null,
                concatenado = concatenado_e || false;

            if (this.N !== null && contenido !== null) {
                if (concatenado) {
                    this.N.innerHTML = this.N.innerHTML + contenido;
                } else {
                    this.N.innerHTML = contenido;
                }
            } else if (this.N !== null && contenido === null) {
                return this.N.innerHTML;
            }
            return this;
        },

        /**
        * Cambia el estado de display de un nodo
        * @param   {Object} a_e 
        *    estado: [Bool = false]
        *    display: [String = block]
        *    oncompleto(estado): [Function]. Ejecuta una función
        *        enviando como parametro el @estado (false:
        *        display=none, true de lo contrario) 
        * @returns {Object} Concatenador de métodos
        */
        mostrar_ocultar: function (a_e) {
            var a = a_e || {},
                estado = (a.estado === undefined)
                    ? (this.N.style.display === "none"
                       || (this.N.scrollHeight === 0 && this.N.scrollWidth === 0))
                        ? true : false
                    : a.estado,
                display = a.display || "block",
                onCompleto = a.onCompleto || function () { return undefined; };

            //Cambiando tipo de display
            this.N.style.display = estado ? display : "none";

            //Si se quiere realizar alguna acción cuando se cambie de estado
            onCompleto(estado);

            return this;
        },

        /**
        * Método para realizar comparativas de tipo
        * @param   {Any}    valor "Valor" para comparar tipo
        * @param   {Object} a_e   
        *                       nulo:[function]
        *                       no_nulo:[function]
        *                       existe_id: [function]
        *                       no_existe_id: [function]
        *                       es_array: [function]
        *                       no_array: [function]
        *                       es_string: [function]
        *                       no_string: [function]
        *                       es_object: [function]
        *                       no_object: [function]
        * @returns {Object} Concatenador de métodos
        */
        nn: function (valor, a_e) {
            var a = {
                es_nulo      : a_e.nulo || function () { return undefined; },
                no_nulo      : a_e.no_nulo || function () { return undefined; },
                existe_id    : a_e.existe_id || function () { return undefined; },
                no_existe_id : a_e.no_existe_id || function () { return undefined; },
                es_array     : a_e.es_array || function () { return undefined; },
                no_array     : a_e.no_array || function () { return undefined; },
                es_string    : a_e.es_string || function () { return undefined; },
                no_string    : a_e.no_string || function () { return undefined; },
                es_object    : a_e.es_object || function () { return undefined; },
                no_object    : a_e.no_object || function () { return undefined; }
            };

            //Null
            if (valor === null) {
                a.es_nulo.apply($c);
            } else {
                a.no_nulo.apply($c);
            }

            //ID
            if (this.$("#" + valor).N === null) {
                a.no_existe_id.apply($c);
            } else {
                a.existe_id.apply($c);
            }

            //Array
            if (!Array.isArray) {
                Array.isArray = function (vArg) {
                    if (Object.prototype.toString.call(vArg) === "[object Array]") {
                        a.es_array.apply($c);
                    } else {
                        a.no_array.apply($c);
                    }
                };
            } else if (Array.isArray(valor)) {
                a.es_array.apply($c);
            } else {
                a.no_array.apply($c);
            }

            //String
            if (Object.prototype.toString.call(valor) === "[object String]") {
                a.es_string.apply($c);
            } else if (typeof valor === "string") {
                a.es_string.apply($c);
            } else {
                a.no_string.apply($c);
            }

            //Object
            if (Object.prototype.toString.call(valor) === "[object Object]") {
                a.es_object.apply($c);
            } else if (typeof valor === "object") {
                a.es_object.apply($c);
            } else {
                a.no_object.apply($c);
            }

            return this;
        },

        /**
        * Obtener keycode
        * @param   {Object} event 
        * @returns {String} Keycode
        */
        key_code: function (event) {
            return event.keyCode || event.which;
        },

        /**
        * Bloque el uso de determinadas teclas a partir del keycode
        * @param   {Object}  event    
        * @param   {Array}   key_code 
        * @returns {Boolean} Bloque las teclas enviadas por medio de key_code
        */
        keylock: function (event, key_code) {
            $c.keydown(function (event) {
                var i;
                for (i = 0; i < key_code.length; i++) {
                    if ($c.key_code(event) === key_code[i]) {
                        $c.Eve.pd(event);
                        return false;
                    }
                }
            });
            return this;
        },

        /**
        * Manejador de eventos
        * Scope: $c.Eve
        * 
        * Metodos:
        *   - listener(listener:[object])
        *     @listener = 
        *          id:[string = null, object scope global]
        *          evento:[string = "load"]
        *          funcion:[function]
        *          tag: [bool = false]
        *          fase: [bool = false]
        *          remover: [bool = false]
        * 
        *   - eliminar(id, evento, funcion) : Elimina los eventos creados 
        *                                     a partir de un handler
        *     @id = [string]
        *     @evento = [string]
        *     @funcion = [function]
        * 
        *   - load(function) : Evento Load
        * 
        *   - to(id, evento, funcion) : Disparador de evento alias de listener
        *     @id = [string]
        *     @evento = [string]
        *     @funcion = [function]
        *   
        *   - no_scroll(activo, event): Bloquea o desbloquea el uso del 
        *                                 scroll o el touchmove
        *     @activo = [bool= false], false para bloquear.
        *     @event 
        * 
        *   - pd() : PreventDefault
        *   - sp() : StopPropagation
        */
        Eve : {
            //Listener para llamado de eventos (private)
            listener: function (a_e) {
                var a = a_e || {},
                    evento  = a.evento || "load",
                    funcion = a.funcion || function () { return undefined; },
                    tag     = a.tag || false,
                    fase    = a.fase || false,
                    remover = a.remover || false,
                    elemento,
                    i;

                function sumando_elementos(ele) {
                    if (window.addEventListener) {
                        ele.addEventListener(evento, funcion, fase);
                    } else if (window.attachEvent) {
                        ele.attachEvent("on" + evento, funcion);
                    }
                }

                function restando_elementos(ele) {
                    if (window.removeEventListener) {
                        ele.removeEventListener(evento, funcion, fase);
                    } else if (window.attachEvent) {
                        ele.detachEvent("on" + evento, funcion);
                    }
                }

                elemento = tag || (($c.N !== null) ? $c.N : window);

                if (elemento !== null) {
                    if (remover) {
                        if (elemento.length) {
                            for (i = 0; i < elemento.length; i++) {
                                restando_elementos(elemento[i]);
                            }
                        } else {
                            restando_elementos(elemento);
                        }
                    } else {
                        if (elemento.length) {
                            for (i = 0; i < elemento.length; i++) {
                                sumando_elementos(elemento[i]);
                            }
                        } else {
                            sumando_elementos(elemento);
                        }
                    }
                }
                return this;
            },

            //Elimina eventos
            eliminar: function (evento, funcion) {
                this.listener({
                    evento  : evento,
                    funcion : funcion,
                    remover : true
                });
                return this;
            },

            //Evento load 
            load: function (funcion) {
                this.listener({funcion: funcion, fase: true});
                return this;
            },

            //Interface simple para eventos
            to: function (evento, funcion) {
                this.listener({
                    evento : evento,
                    funcion: funcion
                });
                return this;
            },

            //Detener el uso de scroll y touchmove en el documento
            no_scroll: function (activo_e, event, manejador) {
                var activo = activo_e || false;

                if (activo) {
                    document.body.style.overflow = "visible";
                    this.eliminar("mousewheel", manejador);
                    this.eliminar("DOMMouseScroll", manejador);
                    this.to("keydown", function () {
                        return true;
                    });
                    this.listener({
                        evento : "touchmove",
                        funcion: manejador,
                        tag: window,
                        remover : true
                    });
                } else {
                    document.body.style.overflow = "hidden";
                    this.to("mousewheel", manejador);
                    this.to("DOMMouseScroll", manejador);
                    $c.keylock(event, [32, 33, 34, 35, 36, 37, 38, 39, 40]);
                    this.listener({
                        evento: "touchmove",
                        funcion: manejador,
                        tag : window
                    });
                }

                return this;
            },

            //PreventDefault
            pd: function (event) {
                if (event.preventDefault) {
                    event.preventDefault();
                } else {
                    event.returnValue = false;
                }
                return false;
            },

            //stopPropagation
            sp: function (event) {
                if (event.stopPropagation) {
                    event.stopPropagation();
                } else {
                    event.cancelBubble = true;
                }
                return false;
            }
        },

        /**
        * Alias
        **/
        toggle: function (a_e) {
            this.mostrar_ocultar(a_e);
            return this;
        },

        load: function (funcion) {
            this.Eve.load(funcion);
            return this;
        },

        keydown: function (funcion) {
            this.Eve.to("keydown", funcion);
            return this;
        },

        click: function (funcion) {
            this.Eve.to("click", funcion);
            return this;
        },

        to: function (evento, funcion) {
            this.Eve.to(evento, funcion);
            return this;
        },

        eliminar_evento: function (evento, funcion) {
            this.Eve.eliminar(evento, funcion);
            return this;
        },

        resize: function (funcion) {
            this.Eve.to("resize", funcion);
            return this;
        }
    };
}());
